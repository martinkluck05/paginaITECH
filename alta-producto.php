<?php
	$titulo = "Alta - Proyecto integrador";
	include 'validar.php';
	
	//Rutina para subir imagenes
?>
</head>
<body>
	<div id="top"><img src="imagenes/top.png" alt="encabezado" width="980" height="80"></div>
	<div id="main">
		<h1><?php echo $titulo ; ?></h1>
		<!-- inicio del desarrollo -->
		
		<?php 
		
			require "conexion.php";
			$fotoAmp = $_FILES['prd_foto1'];
			$fotoMin = $_FILES['prd_foto2'];
			$prd_nombre = $_POST['prd_nombre'];
			$prd_descripcion = $_POST['prd_descripcion'];
			$prd_precio = $_POST['prd_precio'];
			$cat_id = $_POST['cat_id'];
		
			$ruta = "imagenes/";
			$fotoAmpTMP = $fotoAmp['tmp_name'];
			$fotoMinTMP = $fotoMin['tmp_name'];
			
			move_uploaded_file($fotoAmpTMP, $ruta.$fotoAmp['name']);
			move_uploaded_file($fotoMinTMP, $ruta.$fotoMin['name']);
			
			//Rutina para insertar datos en tabla productos
			
			$sql = "INSERT INTO `productos`(`prd_nombre`, `prd_descripcion`, `prd_precio`, `cat_id`, `prd_alta`, `prd_foto1`, `prd_foto2`) 
					VALUES ('".$prd_nombre."','".$prd_descripcion."',".$prd_precio.",".$cat_id.",'".date("Y-m-d")."','".$fotoAmp['name']."','".$fotoMin['name']."')";
			/*if(mysqli_query($link, $sql)){
				echo "Producto ".$prd_nombre." ha sido ingresado correctamente.";
			}
			else{
				echo "No se ha podido agregar ".$prd_nombre." ".mysqli_error($link);
			}*/
			
			mysqli_query($link, $sql);
			
			$chequeo = mysqli_affected_rows($link);
			if($chequeo == 1){
				?>
				<div id="name">
				  <table>
				  	<th colspan="2"><h2>Se ha agregaddo el siguiente producto:</h2></th>
				  	<tr>
				  		<td>Nombre</td>
				  		<td><?php echo  $prd_nombre; ?></td>
				  	<tr>
				  		<td>Descripcion</td>
				  		<td><?php echo $prd_descripcion; ?></td>
				  	<tr>
				  		<td>Precio</td>
				  		<td><?php echo $prd_precio; ?></td>
				  	<tr>
				  		<td>Miniatura</td>
				  		<td><img src="imagenes/<?php echo $fotoMin; ?>" alt="" /></td>
				  	</tr>
				  </table>
				</div>
				<?php
				header("Location:productos.php");
			}
			else{
				echo "No se ha podido agregar ".$prd_nombre." ".mysqli_error($link);
				header("Location:productos.php?error=1");
			}
		 ?>
		<a href="administrar-productos.php">Volver</a>
		
	</div>
	
</body>
</html>