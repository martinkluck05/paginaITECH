<?php session_start(); ?>
<?php
    $usu_id = $_GET['usu_id'];
    $titulo = "Editar - Proyecto integrador";
    require "conexion.php";
    $sql = "SELECT usu_id, usu_login, usu_clave, usu_nombre, usu_apellido, usu_email, usu_tipo FROM usuarios WHERE usu_id=".$usu_id;
                                    
    $resultado = mysqli_query($link, $sql) or die(mysqli_error($link));
    $fila = mysqli_fetch_assoc($resultado);
    
?>
<!DOCTYPE html>
<html lang="es">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ITECH - Importaciones</title>
    <link rel="shortcut icon" href="img/ITECH.ico">
    <link rel="icon" href="img/ITECH.ico">
    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php#page-top">
                    <img src="img/ITECH.svg" alt="" width="200px" style="margin-top: -30px">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="./"><i class="fa fa-home fa-2x" title="Home"></i></a>
                    </li>
                    <li class="page-scroll">
                        <a href="administrar-usuarios.php">Volver</a>
                    </li>
                    <li class="page-scroll">
                        <div class="dropdown">
                          <button class="btn btn-link dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-user fa-2x" title="Usuario"></i>
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <?php 
                                if(!isset($_SESSION['login']) && !isset($_SESSION['nombre'])){ ?>
                                    <li><a class="login" href="#login-modal" data-toggle="modal" data-target="#login-modal">Ingresar</a></li>
                                <?php } 
                                else{   ?>
                                    <li class="text-center"><?php echo $_SESSION['nombre']; ?></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="logout.php">Salir</a></li>
                                <?php } ?>
                            <br>
                            <?php 
                                if(isset($_SESSION['login']) && $_SESSION['login'] == 1){ ?>
                                    <li><a href="administrar-usuarios.php">Administrar Usuarios</a></li>
                            <?php } ?>
                          </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <?php require 'form-login.php'; ?>
    <br>
    <br>
    
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="div-forms">
                        <form id="login-form" action="editar-usuario.php" method="post">
                            <div class="form-group">
                                <input name="usu_login" id="usu_login" class="form-control" type="text" placeholder="Usuario" required value="<?php echo $fila['usu_login']; ?>"><br>
                            <div class="form-goup">
                                <input name="usu_clave" id="usu_clave" class="form-control" type="password" placeholder="Contraseña"><br>                        
                            </div>
                            <div class="form-group">
                                <input type="text" name="usu_nombre" id="usu_nombre" class="form-control" placeholder="Nombre" required value="<?php echo $fila['usu_nombre']; ?>"><br>                        
                            </div>
                            <div class="form-group">
                                <input type="text" name="usu_apellido" id="usu_apellido" class="form-control" placeholder="Apellido" required value="<?php echo $fila['usu_apellido']; ?>"><br>                        
                            </div>
                            <div class="form/group">
                                <input name="usu_email" id="usu_email" class="form-control" type="email" placeholder="Email" required value="<?php echo $fila['usu_email']; ?>"><br>
                            </div>
                            <div class="form-group">
                              <label for="usu_tipo">Tipo de usuario</label>
                              <select class="form-control" id="usu_tipo" name="usu_tipo">
                                <option value="1" <?php if($fila['usu_tipo'] == 1){ echo "selected";} ?>>Administrador</option>
                                <option value="2" <?php if($fila['usu_tipo'] == 2){ echo "selected";} ?>>Usuario</option>
                                <option value="3" <?php if($fila['usu_tipo'] == 3){ echo "selected";} ?>>Empleado</option>
                                <option value="4" <?php if($fila['usu_tipo'] == 4){ echo "selected";} ?>>Otro</option>
                              </select>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="usu_id" value="<?php echo $fila['usu_id']; ?>" id="usu_id"/>
                                <input type="submit" name="enviar" value="Modificar usuario" id="enviar" class="btn btn-primary btn-lg btn-block"/>
                            </div>
                        </form>                                              
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <!--div class="footer-col col-md-6">
                        <ul class="list-inline">
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                        </ul>
                    </div-->
                    <div class="col-lg-12 text-center">
                        <address>
                          <strong>Ivan Benitez</strong><br>
                          <a href="mailto:benitezivo@gmail.com">benitezivo@gmail.com</a><br>
                          <abbr title="Teléfono">Tel:</abbr> +54 11 64511995
                        </address>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; 2015, ITECH Importaciones Todos los derechos Reservados.
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visible-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript 
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

     Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>

    <script type="text/javascript">
        function confirmacion(){
            var mensaje = 'Si pulsa el boton "Aceptar", se eliminara el producto seleccionado.';
            
            if (confirm(mensaje)) {
                return true;
            }
            //redireccion a panel-productos
            window.location='panel-productos.php';
            return false;
        }
    </script>

</body>
</html>