<!DOCTYPE html>
<?php
    require_once "validar.php";
    require "conexion.php";
    session_start();
    $sql = "SELECT prd_id,prd_nombre, prd_descripcion, prd_precio, prd_foto1, prd_foto2, prd_alta FROM productos";
    if (isset($_GET['cat_id'])) {                    
        $cat_id = $_GET['cat_id'];
        $sql.=" WHERE cat_id=".$cat_id;
     }
    if (isset($_GET['precio'])) {
        $precio = $_GET['precio'];
        if ($precio == '>') {
            $sql.=" order by prd_precio ";
        }
        else{
            $sql.=" order by prd_precio DESC ";
        }
    }
    if (isset($_GET['fecha'])) {
        $fecha = $_GET['fecha'];
        if ($fecha == '>') {
            $sql.=" order by prd_alta ";
        }
        else{
            $sql.=" order by prd_alta DESC ";
        }
    }
    $resultado = mysqli_query($link, $sql) or die(mysqli_error($link)); 
    $resultado2 = mysqli_query($link, $sql) or die(mysqli_error($link)); 
    $cantidad = mysqli_num_rows($resultado);
    date_default_timezone_set("America/Argentina/Buenos_Aires");
?>

<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ITECH - Importaciones</title>

    <link rel="shortcut icon" href="img/ITECH.ico">
    <link rel="icon" href="img/ITECH.ico">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/freelancer.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php#page-top">
                    <img src="img/ITECH.svg" alt="" width="200px" style="margin-top: -30px">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="./"><i class="fa fa-home fa-2x" title="Home"></i></a>
                    </li>
                    <li class="page-scroll">
                        <a href="administrar-productos.php">Volver</a>
                    </li>
                    <li class="page-scroll">
                        <div class="dropdown">
                          <button class="btn btn-link dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-user fa-2x" title="Usuario"></i>
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <?php 
                                if(!isset($_SESSION['login']) && !isset($_SESSION['nombre'])){ ?>
                                    <li><a class="login" href="#login-modal" data-toggle="modal" data-target="#login-modal">Ingresar</a></li>
                                <?php } 
                                else{   ?>
                                    <li class="text-center"><?php echo $_SESSION['nombre']; ?></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="logout.php">Salir</a></li>
                                <?php } ?>
                            <br>
                            <?php 
                                if(isset($_SESSION['login']) && $_SESSION['login'] == 1){ ?>
                                    <li><a href="administrar-usuarios.php">Administrar Usuarios</a></li>
                            <?php } ?>
                          </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>


    <?php require 'form-login.php'; ?>

    <br>
    <br>

    <!-- Portfolio Grid Section -->
    <section id="portfolio">
    <?php
        while($fila2 = mysqli_fetch_assoc($resultado2)){
            if(isset($_SESSION['login']) && $_SESSION['login'] == 1){
                if($_SESSION['login'] == 1){ 
    ?>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body text-left">
                            <h2>Modificar Producto</h2>
                            <form action="editar-producto.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                <div class="form-group">
                                <label for="exampleInput">Nombre</label>
                                <input type="text" name="prd_nombre" value="<?php echo $fila2['prd_nombre']; ?>" id="prd_nombre">
                              </div>
                              <div class="form-group">
                                <label for="exampleInput">Descripción</label>
                                <br>
                                <textarea name="prd_descripcion" rows="8" cols="40" id="prd_descripcion"><?php echo $fila2['prd_descripcion']; ?></textarea>
                              </div>
                              <div class="form-group">
                                  <label for="exampleInput">Precio</label>
                                  <input type="text" name="prd_precio" value="<?php echo $fila2['prd_precio']; ?>" id="prd_precio"/>
                              </div>
                              <div class="form-group">
                                  <label for="exampleInput">Categoría</label>
                                  <?php
                                        require "conexion.php";
                                        $sql = "SELECT cat_id, cat_nombre FROM categorias";
                                        
                                        $resultado = mysqli_query($link, $sql) or die(mysqli_error($link));
                                     ?>
                                    <select name="cat_id" id="cat_id">
                                        <?php
                                            $cont=0;
                                            while($fila = mysqli_fetch_assoc($resultado)){ ?>
                                            <option value="<?php echo $fila['cat_id']; ?>"><?php echo $fila['cat_nombre']; ?></option>
                                        <?php } ?>
                                    </select>
                              </div>
                              <div class="form-group">
                                <label for="exampleInputFile">Imagen Miniatura</label>
                                <br>
                                <img src="imagenes/<?php echo $fila2['prd_foto2']; ?>" class="img-responsive img-centered" alt="">
                                <input type="file" name="prd_foto2" value="" id="prd_foto2"/>
                              </div>
                              <br>
                              <div class="form-group">
                                <label for="exampleInputFile">Imagen Ampliada</label>
                                <br>
                                <img src="imagenes/<?php echo $fila2['prd_foto1']; ?>" class="img-responsive img-centered" alt="">
                                <input type="file" name="prd_foto1" value="" id="prd_foto1"/>
                              </div>
                              <br>
                              <input type="hidden" name="prd_id" value="<?php echo $fila2['prd_id']; ?>" id="prd_id" />
                              <button type="submit" class="btn btn-default" name="enviar" id="enviar">Enviar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    <?php } } }
    ?>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <!--div class="footer-col col-md-6">
                        <ul class="list-inline">
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                        </ul>
                    </div-->
                    <div class="col-lg-12 text-center">
                        <address>
                          <strong>Ivan Benitez</strong><br>
                          <a href="mailto:benitezivo@gmail.com">benitezivo@gmail.com</a><br>
                          <abbr title="Teléfono">Tel:</abbr> +54 11 64511995
                        </address>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; 2015, ITECH Importaciones Todos los derechos Reservados.
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visible-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <!-- Portfolio Modals -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript --
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>

    <script type="text/javascript">
        function confirmacion(){
            var mensaje = 'Si pulsa el boton "Aceptar", se eliminara el producto seleccionado.';
            
            if (confirm(mensaje)) {
                return true;
            }
            //redireccion a panel-productos
            window.location='productos.php';
            return false;
        }
    </script>

</body>

</html>
