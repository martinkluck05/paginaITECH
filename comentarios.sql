-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-12-2015 a las 14:00:17
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `itechdb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE IF NOT EXISTS `comentarios` (
  `com_id` int(11) NOT NULL,
  `usu_id` int(11) NOT NULL,
  `com_texto` text NOT NULL,
  `com_fecha` datetime NOT NULL,
  `prd_id` int(11) NOT NULL,
  `com_titulo` varchar(50) NOT NULL,
  `com_puntaje` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`com_id`, `usu_id`, `com_texto`, `com_fecha`, `prd_id`, `com_titulo`, `com_puntaje`) VALUES
(1, 1, 'Me parecio un producto de muy mala calidad y terminacion ademas de caro.', '2015-12-09 21:05:00', 1, 'Muy Malo', 1),
(2, 3, 'Estaria bueno que no mientan con el producto me entregaron algo de muy mala calidad con la caja rota.', '2015-12-09 21:08:00', 1, 'Demasiado malo', 1),
(3, 2, 'Recomendable producto.', '2015-12-09 21:10:00', 1, 'Muy bueno', 5),
(6, 4, 'Estaria bueno que no mientan con el producto me entregaron algo de muy mala calidad con la caja rota.', '2015-12-17 17:04:00', 0, 'Demasiado malo', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`com_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
