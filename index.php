<!DOCTYPE html>
<?php
    require "conexion.php";
    session_start();
    $sql = "SELECT prd_id,prd_nombre, prd_descripcion, prd_precio, prd_foto1, prd_foto2, prd_alta FROM productos";                
    $resultado = mysqli_query($link, $sql) or die(mysqli_error($link)); 
    $resultado2 = mysqli_query($link, $sql) or die(mysqli_error($link)); 
    $cantidad = mysqli_num_rows($resultado);
?>
<html lang="es">            
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ITECH - Importaciones</title>
    <link rel="shortcut icon" href="img/ITECH.ico">
    <link rel="icon" href="img/ITECH.ico">
    <link rel="shortcut icon" type="image/x-icon" href="img/ITECH.ico" />
    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top">
                    <img src="img/ITECH.svg" alt="" width="200px" style="margin-top: -30px">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll menu">
                        <a href="productos.php">Productos</a>
                        <ul>
                            <?php
                                require "conexion.php";
                                $sql = "SELECT cat_id, cat_nombre 
                                        FROM categorias";                                
                                $cat = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                                $cantidad = mysqli_num_rows($cat);
                                while($fila = mysqli_fetch_assoc($cat)){                      
                            ?>
                            <li><a href="productos.php?cat_id=<?php echo $fila['cat_id']; ?>"><?php echo $fila['cat_nombre']; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    </li>
                    <li class="page-scroll">
                        <a href="#portfolio">Novedades</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">Acerca de</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#contact">Contacto</a>
                    </li>
                    <li class="page-scroll">
                        <div class="dropdown">
                          <button class="btn btn-link dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-user fa-2x" title="Usuario"></i>
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <?php 
                                if(!isset($_SESSION['login']) && !isset($_SESSION['nombre'])){ ?>
                                    <li><a class="login" href="#login-modal" data-toggle="modal" data-target="#login-modal">Ingresar</a></li>
                                <?php } 
                                else{   ?>
                                    <li class="text-center"><?php echo $_SESSION['nombre']; ?></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="logout.php">Salir</a></li>
                                <?php } ?>
                            <br>
                            <?php 
                                if(isset($_SESSION['login']) && $_SESSION['login'] == 1){ ?>
                                    <li><a href="administrar-usuarios.php">Administrar Usuarios</a></li>
                            <?php } ?>
                          </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="img/ITECH.svg" alt="" width="30%">
                    <div class="intro-text">
                        <span class="skills">Productos de importación</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Novedades</h2>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <?php
                    $cont=0;
                    while($fila = mysqli_fetch_assoc($resultado)){
                        if(strtotime($fila['prd_alta']) >= mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"))){
                ?>
                <div class="col-sm-4 portfolio-item text-center">
                    <a href="#<?php echo $fila['prd_id']; ?>" class="portfolio-link" data-toggle="modal">
                        <div class="caption">
                            <div class="caption-content">
                                <i class="fa fa-search-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="imagenes/<?php echo $fila['prd_foto1']; ?>" class="img-responsive" alt="" style="margin: 0 auto;">
                        <h2><?php echo $fila['prd_nombre']; ?></h2>
                        <h2>
                            <?php 
                                $sql = "SELECT com_id, com_titulo, com_texto, usu_login, com_fecha, com_puntaje 
                                        FROM comentarios 
                                        LEFT JOIN usuarios ON comentarios.usu_id = usuarios.usu_id
                                        INNER JOIN productos ON comentarios.prd_id = productos.prd_id
                                        WHERE productos.prd_id=".$fila['prd_id'];        
                                $comentarios = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                                $result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                                $cant = mysqli_num_rows($comentarios);            
                                $puntaje = 0;
                                while($com = mysqli_fetch_assoc($result)){      
                                    $puntaje+=$com['com_puntaje'];
                                }       
                                $puntaje = round($puntaje/$cant);    
                                if($puntaje == 1){
                                    echo "★";
                                }elseif ($puntaje == 2) {
                                    echo "★★";
                                }elseif ($puntaje == 3) {
                                    echo "★★★";
                                }elseif ($puntaje == 4) {
                                    echo "★★★★";
                                }elseif ($puntaje == 5) {
                                    echo "★★★★★";
                                } 
                            ?>
                            </h2>
                        <h3>$<?php echo $fila['prd_precio']; ?></h3>
                    </a>
                </div>
                <?php } } ?>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Acerca de</h2>
                </div>
            </div>
            <br>
            <div class="row text-center">
                <div class="col-lg-4">
                    <h3>Quienes somos</h3>
                    <p>ITech es una empresa con amplia experiencia en servicios, atencion al cliente, soporte técnico y venta de insumos.
Nuestro equipo lo forman personas comprometidas y capacitadas. Utilizamos la tecnología no como un fin sino como un medio para ofrecer soluciones competitivas a nuestros clientes.
Participamos activamente en la comunidad y estamos comprometidos con el uso, generación y desarrollo del software libre.</p>
                </div>
                <div class="col-lg-4">
                <h3>Misión</h3>
                    <p>Proveer servicios profesionales para la integracion tecnologica a nuestros clientes y usuarios.</p>
                </div>
                <div class="col-lg-4">
                    <h3>Valores</h3>
                    <ul>
                        <li>-La cordialidad en el trato personal.</li>
                        <li>-La honestidad en nuestras acciones.</li>
                        <li>-El compromiso individual.</li>
                        <li>-El trabajado en equipo.</li>
                        <li>-La excelencia en nuestros servicios.</li>
                        <li>-La capacitacion permanente.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Contacto</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                    <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Nombre</label>
                                <input type="text" class="form-control" placeholder="Nombre" id="name" required data-validation-required-message="Por favor ingrese su nombre.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Email" id="email" required data-validation-required-message="Por favor ingrese su email.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Teléfono</label>
                                <input type="tel" class="form-control" placeholder="Teléfono" id="phone" required data-validation-required-message="Por favor ingrese su numero de teléfono.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Mensaje</label>
                                <textarea rows="5" class="form-control" placeholder="Mensaje" id="message" required data-validation-required-message="Por favor ingrese un mensaje."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <br>
                        <div id="success"></div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <button type="submit" class="btn btn-success btn-lg">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <!--div class="footer-col col-md-6">
                        <ul class="list-inline">
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                        </ul>
                    </div-->
                    <div class="col-lg-12 text-center">
                        <address>
                          <strong>Ivan Benitez</strong><br>
                          <a href="mailto:benitezivo@gmail.com">benitezivo@gmail.com</a><br>
                          <abbr title="Teléfono">Tel:</abbr> +54 11 64511995
                        </address>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; 2015, ITECH Importaciones Todos los derechos Reservados.
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visible-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    <!-- Portfolio Modals -->
    <?php
        while($fila2 = mysqli_fetch_assoc($resultado2)){
    ?>
    <div class="portfolio-modal modal fade" id="<?php echo $fila2['prd_id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2><?php echo $fila2['prd_nombre']; ?></h2>
                            <hr class="star-primary">
                            <img src="imagenes/<?php echo $fila2['prd_foto1']; ?>" class="img-responsive img-centered" alt="">
                            <h2>
                            <?php 
                                $sql = "SELECT com_id, com_titulo, com_texto, usu_login, com_fecha, com_puntaje 
                                        FROM comentarios 
                                        LEFT JOIN usuarios ON comentarios.usu_id = usuarios.usu_id
                                        INNER JOIN productos ON comentarios.prd_id = productos.prd_id
                                        WHERE productos.prd_id=".$fila2['prd_id'];        
                                $comentarios = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                                $result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
                                $cant = mysqli_num_rows($comentarios);            
                                $puntaje = 0;
                                while($com = mysqli_fetch_assoc($result)){      
                                    $puntaje+=$com['com_puntaje'];
                                }       
                                $puntaje = round($puntaje/$cant);    
                                if($puntaje == 1){
                                    echo "★";
                                }elseif ($puntaje == 2) {
                                    echo "★★";
                                }elseif ($puntaje == 3) {
                                    echo "★★★";
                                }elseif ($puntaje == 4) {
                                    echo "★★★★";
                                }elseif ($puntaje == 5) {
                                    echo "★★★★★";
                                } 
                            ?>
                            </h2>
                            <br>
                            <p><?php echo $fila2['prd_descripcion']; ?></p>
                            <ul class="list-inline item-details">
                                <li>Precio:
                                    <strong>$<?php echo $fila2['prd_precio']; ?>
                                    </strong>
                                </li>
                                <li>Fecha de ingreso:
                                    <strong><?php echo date("d-m-Y", strtotime($fila2['prd_alta'])); ?>
                                    </strong>
                                </li>
                            </ul>
                            
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <h2>Comentarios</h2>
                        <br>
                    </div>
                    <div class="col-lg-6 text-left">
                    <?php 
                        if(!isset($_SESSION['login']) && !isset($_SESSION['nombre'])){ ?>
                            Ingresar si desea comentar <a class="login" href="#login-modal" data-toggle="modal" data-target="#login-modal">Ingresar</a>
                         <?php } 
                        else{   ?>
                        <form class="form-horizontal" action="comentar.php" method="post">
                            <fieldset>
                            <!-- Text input-->
                            <div class="form-group">
                              <label class="col-md-6 control-label" for="com_titulo">Título</label>  
                              <div class="col-md-6">
                              <input id="com_titulo" name="com_titulo" type="text" placeholder="" class="form-control input-md">
                                
                              </div>
                            </div>

                            <!-- Textarea -->
                            <div class="form-group">
                              <label class="col-md-6 control-label" for="com_texto">Comentario</label>
                              <div class="col-md-6">                     
                                <textarea class="form-control" id="com_texto" name="com_texto"></textarea>
                              </div>
                            </div>

                            <!-- Select Basic -->
                            <div class="form-group">
                              <label class="col-md-6 control-label" for="com_puntaje">Puntaje</label>
                              <div class="col-md-6">
                                <select id="com_puntaje" name="com_puntaje" class="form-control">
                                  <option value="1">★</option>
                                  <option value="2">★★</option>
                                  <option value="3">★★★</option>
                                  <option value="4">★★★★</option>
                                  <option value="5">★★★★★</option>
                                </select>
                              </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                              <label class="col-md-6 control-label" for="enviar"></label>
                              <div class="col-md-6">
                                <input type="hidden" name="prd_id" value="<?php echo $fila2['prd_id']; ?>" id="prd_id" />
                                <button id="enviar" name="enviar" class="btn btn-primary">Comentar</button>
                              </div>
                            </div>

                            </fieldset>
                        </form>
                        
                    <?php } ?>
                    </div>
                    <div class="col-lg-6" style="border: 3px solid;">
                        <?php
                            while($row = mysqli_fetch_assoc($comentarios)){
                         ?>
                         <h3><?php echo $row['com_titulo']; ?></h3>
                        <h4>
                            <?php echo $row['usu_login']; ?> 
                            | 
                            <?php echo date("d-m-Y G:i", strtotime($row['com_fecha'])); ?>
                        </h4>
                        <p><?php echo $row['com_texto']; ?></p>
                        Puntaje: 
                        <?php 
                                if($row['com_puntaje'] == 1){
                                    echo "★";
                                }elseif ($row['com_puntaje'] == 2) {
                                    echo "★★";
                                }elseif ($row['com_puntaje'] == 3) {
                                    echo "★★★";
                                }elseif ($row['com_puntaje'] == 4) {
                                    echo "★★★★";
                                }elseif ($row['com_puntaje'] == 5) {
                                    echo "★★★★★";
                                } 
                            }
                        ?>
                    </div>
                    <br>
                    <div class="col-lg-12">
                    <br>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php require 'form-login.php'; ?>
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript --
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>

</body>

</html>
