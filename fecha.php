<?php 
	date_default_timezone_set("America/Argentina/Buenos_Aires");

	function obtenerDiaEsp($dia){
	switch ($dia) {
		case 0:
			$semana = "Domingo";
			break;
		
		case 1:
			$semana = "Lunes";
			break;
		
		case 2:
			$semana = "Martes";
			break;
		
		case 3:
			$semana = "Miércoles";
			break;
		
		case 4:
			$semana = "Jueves";
			break;
		
		case 5:
			$semana = "Viernes";
			break;
		
		default:
			$semana = "Sábado";
			break;
	}
	return $semana;
}
?>