            <!-- BEGIN # MODAL LOGIN -->
            <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" align="center">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </button>
                        </div>
                                            
                        <!-- Begin # DIV Form -->
                        <div id="div-forms">
                            <!-- Begin # Login Form -->
                            <form id="login-form" action="login.php" method="post">
                                <div class="modal-body">
                                    <input name="usu_login" id="usu_login" class="form-control" type="text" placeholder="Usuario" required>
                                    <input name="usu_clave" id="usu_clave" class="form-control" type="password" placeholder="Contraseña" required>
                                </div>
                                <div class="modal-footer">
                                    <div>
                                        <input type="submit" value="Enviar" class="btn btn-primary btn-lg btn-block"/>
                                    </div>
                                    <br>
                                    <a class="login" href="#" data-toggle="modal" data-target="#regist-modal">Registrarse</a>
                                </div>
                            </form>
                        <!-- End # Login Form -->                                              
                        </div>
                        <!-- End # DIV Form -->
                    </div>
                </div>
            </div>
            <!-- END # MODAL LOGIN -->
            <div class="modal fade" id="regist-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header" align="center">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </div>
                            <!-- Begin # DIV Form -->
                            <div id="div-forms">
                                <!-- Begin # Login Form -->
                                <form id="login-form" action="registro.php" method="post">
                                    <div class="modal-body">
                                        <input name="usu_login" id="usu_login" class="form-control" type="text" placeholder="Usuario" required><br>
                                        <input name="usu_clave" id="usu_clave" class="form-control" type="password" placeholder="Contraseña" required><br>
                                        <input type="text" name="usu_nombre" id="usu_nombre" class="form-control" placeholder="Nombre" required><br>
                                        <input type="text" name="usu_apellido" id="usu_apellido" class="form-control" placeholder="Apellido" required><br>
                                        <input name="usu_email" id="usu_email" class="form-control" type="email" placeholder="Email" required><br>
                                    </div>
                                    <div class="modal-footer">
                                        <div>
                                            <input type="submit" value="Enviar" class="btn btn-primary btn-lg btn-block"/>
                                        </div>
                                    </div>
                                </form>
                                <!-- End # Login Form -->                                              
                            </div>
                            <!-- End # DIV Form -->
                        </div>
                    </div>
                </div>